## Interface: 80200
## Name: KkthnxUI Config
## Title: |cff4488ffKkthnxUI Config|r
## Notes: Options for KkthnxUI
## Author: Kkthnx
## Version: 9.09
## DefaultState: Enabled
## SavedVariables: KkthnxUIConfigShared, KkthnxUIConfigPerAccount
## SavedVariablesPerCharacter: KkthnxUIConfigNotShared
## RequiredDeps: KkthnxUI

# -- Load Core File
KkthnxUI_Config.lua

# -- Load Locales
Locales\enUS.lua
Locales\zhCN.lua
Locales\deDE.lua
Locales\ruRU.lua